﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UpdateLives : MonoBehaviour {

	public static GameObject thisText;
	public static Text text;
	
	public static GameObject thatText;
	public static Text textTwo;
	
	public static int difficultyByLives = 3;
	
	// Use this for initialization
	void Start () 
	{
		thisText = GameObject.FindWithTag("UpperLeftLife");
		text = thisText.GetComponent<Text>();
		text.text = " = " + difficultyByLives; 
		
		thatText = GameObject.FindWithTag("TransitionScreen");
		textTwo = thatText.GetComponent<Text>();
		textTwo.text = " = " + difficultyByLives;
		
	}
	
	public void BallHasFallenUpdateLives(int changeLivesCount)
	{
		difficultyByLives+= changeLivesCount;
		text.text = " = " + difficultyByLives;
		textTwo.text = " = " + difficultyByLives;
		if(difficultyByLives <= 0)
		{
			Application.LoadLevel ("Lose Screen");
		}
	}
}
