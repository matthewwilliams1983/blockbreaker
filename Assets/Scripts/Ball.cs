﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

	private Paddle paddle;
	private Vector3 paddleToBallVector;
	
	public static bool hasStarted = false;
	public static int ballCount = 0;

	// Use this for initialization
	void Start () 
	{
		ballCount++;
		paddle = GameObject.FindObjectOfType<Paddle>();
		paddleToBallVector = this.transform.position - paddle.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
		if(!hasStarted)
		{	
			//Lock ball relative to paddle.
			this.transform.position = paddle.transform.position + paddleToBallVector;
			
			if(Input.GetMouseButtonDown(0) && paddle.autoPlay)
			{
				hasStarted = true;
				print ("Left mouse button clicked, launch ball: AutoPlay");
				this.GetComponent<Rigidbody2D>().velocity = new Vector2(2.0f, 10f);
			}
			else if(Input.GetMouseButtonDown(0))
			{
				hasStarted = true;
				print ("Left mouse button clicked, launch ball: NormalPlay");
				this.GetComponent<Rigidbody2D>().velocity = new Vector2(2.0f, 10f);
			}
		}
	}
	
	void OnCollisionEnter2D(Collision2D collision)
	{
	
		Vector2 tweak = new Vector2 (Random.Range(0f, 0.2f), Random.Range (0f, 0.2f));
	
		if(hasStarted)
		{
			GetComponent<AudioSource>().Play();
			GetComponent<Rigidbody2D>().velocity += tweak;
		}
	}
}
