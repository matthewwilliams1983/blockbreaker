﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

	public bool showCursor;
	   
	
	private GameObject transitionScreenText;
	private GameObject transitionScreenBallImage;
	private GameObject theCanvas;
	public Vector3 textVector;
	public Vector3 ballImageVector;
	public Vector3 canvasVector;
		
	public void LoadLevel(string name)
	{
		Brick.breakableCount = 0;
		UpdateLives.difficultyByLives = 3;
		Ball.hasStarted = false;
		Debug.Log ("Level load requested for: " + name);
		Application.LoadLevel(name);
		
	}
	public void QuitLevel()
	{
		Debug.Log ("Quitting");
		Application.Quit ();
	}
	
	public void LoadNextLevel()
	{
		Brick.breakableCount = 0;
		Ball.ballCount = 0;
		Ball.hasStarted = false;
		Application.LoadLevel (Application.loadedLevel + 1);
	}
	
	public void BrickDestroyed()
	{
		//this is the last brick
		if(Brick.breakableCount <= 0)
		{
			if(Application.loadedLevelName == "Level_08")
			{
				Brick.breakableCount = 0;
				Ball.ballCount = 0;
				Ball.hasStarted = false;
				Application.LoadLevel("Start");
			}
			else
			{
				LoadNextLevel();
			}
		}
	}
	
	public void Start()
	{
		if(Application.loadedLevelName == "Start" || Application.loadedLevelName == "Lose Screen" || Application.loadedLevelName == "Win")
		{
			Cursor.visible = true;
		}
		else
		{
			Cursor.visible = showCursor;
		}
		
		
		if(Application.loadedLevelName != "Start" && Application.loadedLevelName != "Win" && Application.loadedLevelName != "Lose Screen")
		{
			//Get the normal position of the transition screen then move it
			transitionScreenText = GameObject.FindGameObjectWithTag("TransitionScreen");
			textVector = transitionScreenText.transform.localPosition;
			transitionScreenBallImage = GameObject.FindGameObjectWithTag("BallImage");
			ballImageVector = transitionScreenBallImage.transform.localPosition;
			theCanvas = GameObject.FindGameObjectWithTag("TransCan");
			canvasVector = theCanvas.transform.localPosition;
			
			//Move it
			transitionScreenBallImage.transform.localPosition = new Vector3(1000, 1000, 0);
			transitionScreenText.transform.localPosition = new Vector3(1000, 1000, 0);
			theCanvas.transform.localPosition = new Vector3(1000, 1000, 0);
		}
		
	}
}
