﻿using UnityEngine;
using System.Collections;

/*
 	This class defines how capsules work that contain power ups
*/
public class Capsule : MonoBehaviour {

	//Public variables
	public enum PowerUp{biggerPaddle, extraBallSpawn, loseALife, extraLife, biggerBall};
	public PowerUp powerUpTypeOfThisCapsule;
	public GameObject newBall;
	
	//Private variables
	private Paddle paddle;
	private Ball hugeBall;
	private UpdateLives updateLives;
	private bool didCollideBiggerPaddle = false;
	
	private int powerUpTimerPaddleLength = 1300;
	private int powerUpColorChange = 0;
	private float rotationSpeedCapsule = 75.0f;
	
	// Use this for initialization of a Capsule
	void Start () {
		
		//Get the paddle in the scene
		paddle = GameObject.FindObjectOfType<Paddle>();
		updateLives = GameObject.FindObjectOfType<UpdateLives>();
	}
	
	// Update is called once per frame if a Capsule has been instantiated
	void Update () 
	{
		
		if(this.powerUpTypeOfThisCapsule == PowerUp.extraLife)
		{
			ChangeColorOfExtraLifeBall();
		}
		else
		{
			//Rotate capsule about the z axis
			this.transform.Rotate(0, 0, rotationSpeedCapsule * Time.deltaTime, Space.World);	
		}
		
		//Start the timer for the power up of bigger paddle
		if(didCollideBiggerPaddle)
		{
			TimerForBiggerPaddle();
		}

	}
	
	//Determine what kind of capsule has collided with the paddle
	void OnCollisionEnter2D(Collision2D collision)
	{
		if(this.powerUpTypeOfThisCapsule == PowerUp.biggerPaddle)//BiggerPaddle
		{	
			BiggerPaddle();
		}
		else if(this.powerUpTypeOfThisCapsule == PowerUp.extraBallSpawn)//SpawnExtraBall
		{
			ExtraBallSpawn();
		}
		else if(this.powerUpTypeOfThisCapsule == PowerUp.loseALife)//InstantDeath
		{
			InstantDeath();
		}
		else if(this.powerUpTypeOfThisCapsule == PowerUp.extraLife)//ExtraLife
		{
			ExtraLife();
		}
		else if(this.powerUpTypeOfThisCapsule == PowerUp.biggerBall)//BiggerBall
		{
			BiggerBall();
		}

	}
	
	void ChangeColorOfExtraLifeBall()
	{
		if(powerUpColorChange % 10 == 0)
		{
			this.GetComponent<SpriteRenderer>().color = Color.white;
		}
		else if(powerUpColorChange % 5 == 0)
		{
			this.GetComponent<SpriteRenderer>().color = Color.yellow;
		}
		powerUpColorChange++;
	}
	
	void TimerForBiggerPaddle()
	{
		if(powerUpTimerPaddleLength >= 0 && didCollideBiggerPaddle == true)
		{
			powerUpTimerPaddleLength--;	
		}
		else
		{
			paddle.GetComponent<Transform>().localScale = new Vector3(paddle.GetComponent<Transform>().localScale.x - 1, 1, 1);
			paddle.keepPaddleInPlayLeft = paddle.keepPaddleInPlayLeft - 1.1f;
			paddle.keepPaddleInPlayRight = paddle.keepPaddleInPlayRight + 1.1f;	
			didCollideBiggerPaddle = false;

			powerUpTimerPaddleLength = 1300;
			
		}
	}
	
	
	void BiggerPaddle()
	{
		GetComponent<AudioSource>().Play ();
		paddle.GetComponent<Transform>().localScale = new Vector3(paddle.GetComponent<Transform>().localScale.x + 1, 1, 1);
		didCollideBiggerPaddle = true;

		this.transform.position = new Vector3(1000, 0, 0);
		paddle.keepPaddleInPlayLeft = paddle.keepPaddleInPlayLeft + 1.1f;
		paddle.keepPaddleInPlayRight = paddle.keepPaddleInPlayRight - 1.1f;
	}
	
	void ExtraBallSpawn()
	{
		GetComponent<AudioSource>().Play();
		this.transform.position = new Vector3(1000, 0, 0);
		float changeY = paddle.transform.position.y + 0.3f;
		Vector3 newBallSpawnLoc = new Vector3(paddle.transform.position.x, changeY, 0.0f);
		GameObject powerBall = (GameObject) Instantiate(newBall, newBallSpawnLoc, Quaternion.identity);
		powerBall.GetComponent<Rigidbody2D>().velocity = new Vector2(Random.Range (-3f, 3f), 10f);
	}
	
	void InstantDeath()
	{
		GetComponent<AudioSource>().Play ();
		this.transform.position = new Vector3(1000, 0, 0);
		updateLives.BallHasFallenUpdateLives(-1);
	}
	
	void ExtraLife()
	{
		GetComponent<AudioSource>().Play ();
		this.transform.position = new Vector3(1000, 0, 0);
		updateLives.BallHasFallenUpdateLives(1);
	}
	
	void BiggerBall()
	{
		GetComponent<AudioSource>().Play ();
		this.transform.position = new Vector3(1000, 0, 0);
		hugeBall = GameObject.FindObjectOfType<Ball>();
		hugeBall.GetComponent<Transform>().localScale = new Vector3 (hugeBall.GetComponent<Transform>().localScale.x + 1 , hugeBall.GetComponent<Transform>().localScale.y + 1, 1); 
	}
}
