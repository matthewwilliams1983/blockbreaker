﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoseCollider : MonoBehaviour {

	private LevelManager levelManager;
	private Capsule capsule;
	private GameObject transitionScreenText;
	private GameObject transitionScreenBallImage;
	private GameObject theCanvas;
	private GameObject upperLeftLives;
	private GameObject smallBallImage;
	private UpdateLives updateLives;
	private bool isTimedOut = false;
	private int timer = 0;
	private Vector3 homeOfUpperLives;
	private Vector3 homeOfSmallBallImage;

	void Start()
	{
		levelManager = GameObject.FindObjectOfType<LevelManager>();
		updateLives = GameObject.FindObjectOfType<UpdateLives>();
		transitionScreenText = GameObject.FindGameObjectWithTag("TransitionScreen");
		transitionScreenBallImage = GameObject.FindGameObjectWithTag("BallImage");
		theCanvas = GameObject.FindGameObjectWithTag("TransCan");
		upperLeftLives = GameObject.FindGameObjectWithTag("UpperLeftLife");
		smallBallImage = GameObject.FindGameObjectWithTag("SmallBallImage");
		homeOfUpperLives = upperLeftLives.transform.localPosition;
		homeOfSmallBallImage = smallBallImage.transform.localPosition;
	}
	
	void Update()
	{
	
		if(isTimedOut)
		{
			if(timer > 0)
			{
				Time.timeScale = 0;
				Screen.lockCursor = true;
				timer --;
			}
			else
			{
				Time.timeScale = 1;
				Screen.lockCursor = false;
				transitionScreenBallImage.transform.localPosition = new Vector3(1000, 1000, 0);
				transitionScreenText.transform.localPosition = new Vector3(1000, 1000, 0);
				theCanvas.transform.localPosition = new Vector3(1000,1000,0);
				smallBallImage.transform.localPosition = homeOfSmallBallImage;
				upperLeftLives.transform.localPosition = homeOfUpperLives;
				isTimedOut = false;
			}
		}
	}
	
	void OnTriggerEnter2D(Collider2D trigger)
	{

		if(trigger.tag != "PowerUp")
		{
		
			if(Ball.ballCount > 1)
			{
			   Destroy(trigger.gameObject);
			}
			
			Ball.ballCount--;
			
			if(Ball.ballCount <= 0)
			{
				
					updateLives.BallHasFallenUpdateLives(-1);
					
					if(UpdateLives.difficultyByLives <= 0)
					{
						Debug.Log ("Game Over");//Basically do nothing but need an if statement to ensure stuff doesnt get reset
					}
					else
					{
						ArrageScreenForTransition();
					}
			}
			
		}
	}
	
	void ArrageScreenForTransition()
	{
	
		//Move UI GameObjects around to set scene
		transitionScreenText.transform.localPosition = levelManager.textVector;
		transitionScreenBallImage.transform.localPosition = levelManager.ballImageVector;
		theCanvas.transform.localPosition = levelManager.canvasVector;
		smallBallImage.transform.localPosition = new Vector3(1000, 0, 0);
		upperLeftLives.transform.localPosition = new Vector3(1000, 0, 0);
		
		//Set timeout timer and reset the ball for next play
		timer = 200;
		isTimedOut = true;			
		Ball.hasStarted = false;
		Ball.ballCount++;
	}
}
