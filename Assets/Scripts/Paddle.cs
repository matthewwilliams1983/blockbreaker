﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour {

	public bool autoPlay = false;
	public float keepPaddleInPlayLeft;
	public float keepPaddleInPlayRight;
	
	private Ball ball;
	float mousePosInBlocks;
	Vector3 paddlePos;
	
	// Use this for initialization
	void Start () {
		ball = GameObject.FindObjectOfType<Ball>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!autoPlay)
		{
			MoveWithMouse();
		}
		else
		{
			AutoPlay();
		}
	}
	
	void AutoPlay()
	{
		mousePosInBlocks = Input.mousePosition.x / Screen.width * 16;
		Vector3 ballPos = ball.transform.position;
		paddlePos = new Vector3(Mathf.Clamp (ballPos.x,keepPaddleInPlayLeft, keepPaddleInPlayRight), this.transform.position.y, this.transform.position.z);
		this.transform.position = paddlePos;
	}
	
	void MoveWithMouse()
	{
		mousePosInBlocks = Input.mousePosition.x / Screen.width * 16;
		
		paddlePos = new Vector3(Mathf.Clamp (mousePosInBlocks,keepPaddleInPlayLeft, keepPaddleInPlayRight), this.transform.position.y, this.transform.position.z);
		this.transform.position = paddlePos;
	}
}
