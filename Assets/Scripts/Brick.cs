﻿using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour {

	public static int breakableCount = 0;
	public Sprite[] hitSprites;
	public AudioClip crack;
	public GameObject smoke;
	public Capsule capsule;
	
	private int timesHit;
	private int powerUpColorChange = 0;
	private LevelManager levelManager;
	private bool isBreakable;
	private bool isPowerUp;
	
	// Use this for initialization
	void Start () {
		
		isBreakable = (this.tag == "Breakable");
		isPowerUp = (this.tag == "PowerUp");
		
		//Keep track of breakable bricks
		if(isBreakable || isPowerUp)
		{
			breakableCount++;
		}
		
		levelManager = GameObject.FindObjectOfType<LevelManager>();
		timesHit = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(this.tag == "PowerUp" && (powerUpColorChange % 10 == 0))
		{
			this.GetComponent<SpriteRenderer>().color = Color.blue;
		}
		else if(this.tag == "PowerUp" && (powerUpColorChange % 5 == 0))
		{
			this.GetComponent<SpriteRenderer>().color = Color.magenta;
		}
		powerUpColorChange++;
	}
	
	void OnCollisionEnter2D(Collision2D col)
	{


		AudioSource.PlayClipAtPoint(crack, transform.position, 1.0f);


		
		if(isBreakable || isPowerUp)
		{
			Handlehits();
		}
	}
	
	void Handlehits() 
	{
		timesHit++;
		int maxHits = hitSprites.Length + 1;
		
		if(timesHit >= maxHits)
		{
		
			breakableCount--;
			levelManager.BrickDestroyed();
			
			//Call smoke method
			PuffSmoke();
			
			if(isPowerUp)
			{
				GenerateCapusle();
			}		
			
			Destroy(this.gameObject);
	
		}
		else
		{
			LoadSprites();
		}
	}
	
	void PuffSmoke()
	{
		//Make smoke for brick destruction
		GameObject smokePuff = Instantiate(smoke, transform.position, Quaternion.identity) as GameObject;
		smokePuff.GetComponent<ParticleSystem>().startColor = this.GetComponent<SpriteRenderer>().color;
	}
	
	void GenerateCapusle()
	{
		//Capsule code
		powerUpColorChange = 0;
		capsule.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, -3.0f);
		Instantiate(capsule);
	}
	
	void LoadSprites()
	{
		int spriteIndex = timesHit - 1;
		if(hitSprites[spriteIndex])
		{
			this.GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
		}
		else
		{
			Debug.LogError("Brick sprite missing");
		}
	}
	

}
